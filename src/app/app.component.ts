import { Component, OnInit } from '@angular/core';
import { FormControl, FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { ConfirmedValidator } from './confirmed.validator';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{

  // define all properties and array to access in templat
  registerForm: FormGroup;
  countryList : any = ['Cambodia', 'USA', 'Japan','Korean'];
  optionHearList: Array<any> = [
    { name: 'Google', value: 'Google' },
    { name: ' Blog post', value: ' Blog post' },
    { name: ' a firend or university', value: ' a firend or university' },
    { name: 'News articles', value: 'News articles' },
  ];

  monthList : any=[
    'January',
    'February',
    'March',
    'April',
    'May',
    'June'
  ];

  dayList : any=[1,2,3,4,5,6,6,7];
  yearList : any=[1999,2000,2001,2002];


  //inject service
  constructor(private fb : FormBuilder){}
  ngOnInit(): void {

      //define fields and validator method in the second item
      this.registerForm = this.fb.group({
        firstName : ['',  [Validators.required, Validators.maxLength(32)]],
        lastName : ['',  [Validators.required, Validators.maxLength(32)]],
        email : ['',  [Validators.required, Validators.email]],
        password: ['',  [Validators.required, Validators.minLength(8), Validators.maxLength(128)]],
        confirm_password: ['',  [Validators.required, Validators.minLength(8), Validators.maxLength(128)]],
        address : ['',  [Validators.required]],
        areaCode : ['',[Validators.required, Validators.pattern("[0]{1}[0-9]{2}")]],
        phoneNumber : ['',[Validators.required, Validators.pattern("[0-9]{7}")]],
        city : ['',  [Validators.required]],
        state : ['',  [Validators.required]],
        zipCode : ['',[Validators.required, Validators.pattern("[0-9]{6,9}")]],
        country : ['',  [Validators.required]],
        checkArray: this.fb.array([], [Validators.required]),
        accepted: [false, Validators.requiredTrue],
        acceptedRule: [false, Validators.requiredTrue],
        month : ['',  [Validators.required]],
        day : ['',  [Validators.required]],
        year : ['',  [Validators.required]],
      },
      { 
        validator: ConfirmedValidator('password', 'confirm_password')
      }
      )
  }


    //when click submit
    onSubmit():void{
          alert(JSON.stringify(this.registerForm.value,null,5))
     }

    //create getter method it easy to access data in view
    get f() { return this.registerForm.controls; }
    
    
    //handle on chance of input type check
    onCheckboxChange(e) {
      const checkArray: FormArray = this.registerForm.get('checkArray') as FormArray;
  
      if (e.target.checked) {
        checkArray.push(new FormControl(e.target.value));
      } else {
        let i: number = 0;
        checkArray.controls.forEach((item: FormControl) => {
          if (item.value == e.target.value) {
            checkArray.removeAt(i);
            return;
          }
          i++;
        });
      }
    }

    onReset() {
      // this.submitted = false;
      this.registerForm.reset();
    }

}